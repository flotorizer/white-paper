# Flotorizer

## Executive Summary

Throughout the times and dawning of new processes, we have witnessed human error and exploitation on numerous occasions. Speed in trust will have to define a new meaning as commerce expands beyond the 21st century while also not leaving pass processes behind.
Flotorizer allows timestamping into the FLO blockchain creating Proof of Existence documentation and interaction with the Open Index Protocol. The document is then pinned onto Interplanetary File System, and may be retrieved through encryption. This allows users to transfer private information with minimal exposure.

### Purpose

Flotorizer aims to be solution to numerous industries including Legal matters, Finance, Journaling, Estate Planning and etc.
The purpose it serves is being a service that allows users and companies to prove existence of their product externally or within the blockchain itself.  

### Key contributors

Davi Ortega

Pankaj S Y

Anthony Howl

## Why?

The amount of ways PoE can be utilized is boundless with similar alternatives through the bitcoin blockchain, and factom but what makes flotorizer unique is the core fundamentals. FLO's main use-case is floData, unlike other chains who support complex smart contract languages and operations unrelated to storing data. Sometimes it's best to use a tool that's meant to perform best at a single job rather than bringing around a Swiss army knife and using it for only 1 of its functions.

Some use examples being:

- Demonstration of data ownership without revealing actual data

- Proving ownership and Deed transfer

- Asset & Commodity realization

- Checks and balances for document integrity 

- not limited as the use cases expand with how far imagination itself 


### What is FloData?

1.  FloData is accessible and easy to read. You don't need to decompile a smart contract or have a special software to read it. It's in plaintext on the chain. This helps with auditing and explaining how it works to regulators. Any extra steps are a barrier to adoption.

2.  FloData is easy to write. It is appended to the blockchain using a simple RPC command. This makes maintaining the software around the solution less costly.

3. FLO is a fairly launched blockchain with no premine or ICO. Again, this helps for practical regulatory reasons, but also is a philosophical choice made by those who maintain the software. It guides the decision making for our software and also allows those implementing it to check off a box they couldn't check using another chain without those features.

4. FLO is secured by proof of work. This is arguably the best security a network can get, requiring actual buy in of physical devices and constant operational cost in both manpower and electric power to maintain the blockchain.

5. FLO transactions are cheap and fast. This will always be the case - we will likely increase block size as the network grows.

### What is the Open Index Protocol?

### What is the Interplanetary File System(IPFS)?

## Pricing

    PPTS - Pay Per Timestamp & Enterprise Solutions

    Subscription Based &
    Enterprise Solutions

## RoadMap

API Creation

Production Launch for projects to include us within their stack

Incubator for Business Development tools and DAPP Creation / Integration 

## Funding rounds

We shall be promoting a new paradigm of project incentive through utilizing the open collective platform for financial transparency and coordination. The community will be informed through every phase of growth that we experience. Open Collective will also be utilized when we start our incubation period.

## Conclusion

## Reference
